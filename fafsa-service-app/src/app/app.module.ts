import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FafsaServiceLibModule } from 'fafsa-service-lib';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FafsaServiceLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
