import {Component, OnInit} from '@angular/core';
import {FafsaServiceLibService} from "fafsa-service-lib"

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'fafsa-service-app';
    messageFromServiceLib: string;
    schools = 'My Schools';

    constructor (
        private fafsaServiceLibService: FafsaServiceLibService) {

    }

    ngOnInit () {
        this.messageFromServiceLib = this.fafsaServiceLibService.getInfo();
    }
}
