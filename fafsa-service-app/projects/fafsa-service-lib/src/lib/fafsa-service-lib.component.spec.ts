import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FafsaServiceLibComponent } from './fafsa-service-lib.component';

describe('FafsaServiceLibComponent', () => {
  let component: FafsaServiceLibComponent;
  let fixture: ComponentFixture<FafsaServiceLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FafsaServiceLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FafsaServiceLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
