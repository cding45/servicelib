import { TestBed } from '@angular/core/testing';

import { FafsaServiceLibService } from './fafsa-service-lib.service';

describe('FafsaServiceLibService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FafsaServiceLibService = TestBed.get(FafsaServiceLibService);
    expect(service).toBeTruthy();
  });
});
