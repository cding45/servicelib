import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class FafsaServiceLibService {

    constructor () {}

    getInfo(): string {
        return "Updated message from fafsa service library";
    }
}
