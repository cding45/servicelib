import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector: 'lib-fafsa-service-lib',
    template: `
    <p>
      fafsa-service-lib works!
    </p>
    <span [innerHTML]="schools"></span>
  `,
    styles: []
})
export class FafsaServiceLibComponent implements OnInit {

    @Input() schools: string;
    
    constructor () {}

    ngOnInit () {
    }

}
