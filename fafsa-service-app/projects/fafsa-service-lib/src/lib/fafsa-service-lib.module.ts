import { NgModule } from '@angular/core';
import { FafsaServiceLibComponent } from './fafsa-service-lib.component';

@NgModule({
  declarations: [FafsaServiceLibComponent],
  imports: [
  ],
  exports: [FafsaServiceLibComponent]
})
export class FafsaServiceLibModule { }
